<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<!-- <title>
		<?php
			global $query_string;

			if ( is_home() )
				bloginfo( 'name' );

			if ( get_search_query() )
				echo 'Results for: "' . get_search_query() .'"';

			if ( is_month() )
				the_time('F Y');

			if ( is_category() )
				single_cat_title();

			if ( is_single() )
				the_title();

			if ( is_page() )
				the_title();

			if ( is_tag() )
				single_tag_title();

			if ( is_404() )
				echo 'Page Not Found!';
		?>
	</title> -->

	<!-- from mobilepress starter -->
	<meta name="generator" content="WordPress and MobilePress" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
	<link href="<?php bloginfo( 'template_url' ); ?>/style.css" rel="stylesheet" type="text/css" media="screen, handheld, print, projection" />
	<link rel="icon" type="image/gif" href="<?php bloginfo( 'template_url' ); ?>/img/favicon.gif" />
	<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<script type="text/javascript">window.addEventListener('load',function(){setTimeout(function(){window.scrollTo(0, 0);},0);});</script>

	<!-- from bildton theme -->
	<meta name="geo.placename" content="Moosacher Straße 81, 80809 München, Deutschland" />
	<meta name="geo.position" content="48.185788;11.552579" />
	<meta name="geo.region" content="DE-Bayern" />
	<meta name="ICBM" content="48.185788, 11.552579" />
	<meta name="google-site-verification" content="1GnuQUNlKVxBoS2cO-gTgCANdmFRfM7Vd_Gs4f2RN84" />
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width" />
	<meta http-equiv="Access-Control-Allow-Origin" content="*">
	<title><?php wp_title( '' );?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<link href="http://fonts.googleapis.com/css?family=Didact Gothic&subset=latin" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/nivo-slider.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/flexslider.css" type="text/css" media="screen" />
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/pixastic.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.desaturate.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.nivo.slider.pack.js" ></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.flexslider.js" ></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/menu.js" ></script>
	<script>
	(function($) {
	    $(function() {
	        //$('.nocolor').desaturate();

	<?php if(is_front_page()): ?>
			$('#slider').nivoSlider({
				effect: 'random', // Specify sets like: 'fold,fade,sliceDown'
				slices: 15, // For slice animations
				boxCols: 8, // For box animations
				boxRows: 4, // For box animations
				animSpeed: 500, // Slide transition speed
				pauseTime: 3000, // How long each slide will show
				directionNav: false, // Next & Prev navigation
				controlNav: false, // 1,2,3... navigation
			});
	<?php endif; ?>
			partner_slider = $('#partner-slider');
			if(partner_slider.length){
				partner_slider.nivoSlider({
					effect: 'fade', // Specify sets like: 'fold,fade,sliceDown'
					animSpeed: 500, // Slide transition speed
					pauseTime: 3000, // How long each slide will show
					directionNav: false, // Next & Prev navigation
					controlNav: true, // 1,2,3... navigation ,
					manualAdvance: false
				});
			}

		    $('#sale-slider').flexslider({
			    animation: "fade",
			    controlNav: true,
			    directionNav: false,
			    animationLoop: true,
			    //smoothHeight: true,
			    slideshow: true
		    });


		    /*sales_slider = $('#sale-slider');
			if(sales_slider.length){
				sales_slider.nivoSlider({
					effect: 'fade', // Specify sets like: 'fold,fade,sliceDown'
					animSpeed: 500, // Slide transition speed
					pauseTime: 3000, // How long each slide will show
					directionNav: false, // Next & Prev navigation
					controlNav: true, // 1,2,3... navigation ,
					manualAdvance: false
				});
			}*/

		    jQuery(".newsletter-req").fancybox({
			    ajax: {
				    type: "POST",
				    data: 'mydata=test'
			    },
			    width: 800,
			    height: 600,
			    onComplete	:	function() {
				    console.log(this)
				    var email = $('#footer-newsletter input[type="text"]').val();
				    var geb = $('input#newsletter-geb').attr('checked');
				    var ne = $('input#newsletter-new').attr('checked');
				    $('#fancybox-content form input[type="text"]').val(email);
				    if(geb){
					    $('#fancybox-content #newsletter_1').attr('checked','checked');
				    }
				    if(ne){
					    $('#fancybox-content #newsletter_2').attr('checked','checked');
				    }
			    }
		    });
	    });
	})(jQuery);
	</script>
</head>
<body>

<div id="headerwrap">
  <div class="header-top">
    <a href="<?php echo get_home_url(); ?>">bildton.de</a>
  </div>
	<div id="header">
		<div class="header-inner">
			<?php if(!(is_front_page())): ?>
			<a href="<?php echo get_home_url(); ?>">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/arrow.svg" alt="arrow" class='back-arrow'>
			</a>
			<?php endif; ?>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/bitmap.png" alt="logo" class='logo'>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/hamburger.png" alt="menu icon" class='burg'>

			<div class="kategorien-wrapper">
				<p>Kategorien / Suche</p>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/arrow.svg" alt="arrow" class='right-arrow spin-arrow-down'>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/ex.svg" alt="close kategorien" class='ex right-arrow'>
			</div>
		</div>
	</div>
	<nav class="kategorien-dropdown">
		<?php
		get_search_form();
		wp_nav_menu( array( 'theme_location' => 'sidebar', 'container' => '' ) ); ?>
	</nav>
	<nav class='mobile-menu'>
		<?php wp_nav_menu( array( 'theme_location' => 'primary', 'container_id' => 'body-nav' ) ); ?>
	</nav>
</div>

<?php if(is_front_page() || is_archive() || is_single()): ?>
			<div id="slider" class="nivoSlider">
				<?php $slider = get_field('slide');
					if($slider){
						foreach($slider as $k => $slide){
							$image = $slide['sliede_image'];
							if($image){?>
							<a class="ajax" href="<?php echo $slide['sliede_link_to_page'];?>"><img src="<?php echo $image['sizes']['home-slider']; ?>" width="700" height="290" title="#htmlcaption-<?php echo $k; ?>"/></a>
				<?php		}else{ ?>
							<a class="ajax" href="<?php bloginfo('url');?>/kameras/sony-pdw-700/"><img src="<?php echo get_template_directory_uri(); ?>/images/slider-1.jpg" width="700" height="290" title="#htmlcaption-1"/></a>
				<?php		}
						}
					}else{ ?>
					<a class="ajax" href="<?php bloginfo('url');?>/kameras/sony-pdw-700/"><img src="<?php echo get_template_directory_uri(); ?>/images/slider-1.jpg" width="700" height="290" title="#htmlcaption-1"/></a>
				<?php } ?>
			</div>
			<?php $slider = get_field('slide');
					if($slider){
						foreach($slider as $k => $slide){ ?>
							<div class="nivo-caption" id="htmlcaption-<?php echo $k; ?>">
								<div class="slider-caption-title"><?php echo $slide['sliede_title'];?></div>
								<p><?php echo $slide['sliede_subtitle'];?></p>
							</div>
			<?php		}
					}else{ ?>
			<div class="nivo-caption" id="htmlcaption-1">
				<div class="slider-caption-title">SONY PDW 700</div>
				<p>nur 320 € / Tag</p>
			</div>
			<div class="nivo-caption" id="htmlcaption-2">
				<div class="slider-caption-title">Sony PMW 350</div>
				<p>nur 240 € / Tag</p>
			</div>
			<div class="nivo-caption" id="htmlcaption-3">
				<div class="slider-caption-title">Fujinon 4,5mm HD Weitwinkel Zoom</div>
				<p>nur 160 € / Tag</p>
			</div>
			<?php } ?>
			<div class="seperator"></div>
			<div id="label">
				<div class="label-inner">
					<div class="label-box">
						<img src="<?php echo get_template_directory_uri(); ?>/images/phone.png" width="63" height="63" />
						<div class="label-text">BUCHEN UNTER<br /><span>+49 89 / 452 4499 0</span></div>
						<div class="clearfix"></div>
					</div>
					<div class="label-box">
						<a href="<?php if(get_field('pdf_file')) echo the_field('pdf_file');?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/pdf.png" width="63" height="63" /></a>
						<div class="label-text">AKTUELLE PREISE<br /><span class=""><a href="<?php if(get_field('pdf_file')) echo the_field('pdf_file');?>" target="_blank">PDF Download</a></span></div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
<?php endif; ?>

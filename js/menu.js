jQuery(document).ready(function($) {
  //menu
  $('.burg').on('click', function() {
    $('.mobile-menu').slideToggle(200);
  });

  //kategorien menu
  $('.kategorien-wrapper').on('click', function() {
    $('.header-inner').toggleClass('kategorien-open-header');
    $('.kategorien-dropdown').slideToggle(500);
    setTimeout(function() {
      $('.kategorien-wrapper > .right-arrow').toggle(700);
    }, 150);
  });

  //single product info-selector
  $('.info-selector').on('click', function() {
    $(this).children('.single-product-content').slideToggle(500);
    $(this).find('.right-arrow').toggleClass('spin-arrow-down');
  });

  //single product image selector
  $('.img-selector').on('click', function() {
    var selected = $(this).attr('src');
    $('#galery-full').attr('src', selected);
    $('.img-selector').removeClass('img-selected');
    $(this).addClass('img-selected');
  });

  //show/hide home page content
  $('.read-more').on('click', function() {
    $('.entry-content-home').toggleClass('entry-content-open');
  });
});

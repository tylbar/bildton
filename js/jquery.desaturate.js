/**
 *  jquery.desaturation.js
 *
 *  Binds events and effects to the desaturation of images
 *  
 *  Author: Joshua Gildart
 *  Dependencies: Pixastic Desaturate http://www.pixastic.com/lib/git/pixastic/actions/desaturate.js
 */
(function($, window, undefined) {
    if(typeof $ == "undefined") {
        throw "jquery.desaturation.js requires the jQuery library";
    }
    if(typeof Pixastic == "undefined" || !Pixastic.Actions.desaturate)  {
        throw "jquery.desaturation.js requires the Pixastic Desaturate library";
    }
    
    var Desaturate = function(image, options) {
        var self = this;
        
        this.settings = {};
        this.defaults = {
            speed: 600,
            wrapperClass: 'saturation-wrapper',
            saturatedClass: 'colorful',
            desaturatedClass: 'colorless'
        };
        
        this.init = function() {
            this.saturated.addClass(this.settings.saturatedClass);
            this.desaturated = this.generateDesaturatedImage(this.saturated);
            this.desaturated.addClass(this.settings.desaturatedClass);
            
            this.wrapper = 
                $('<span />')
                    .addClass('saturation-wrapper')
                    .css({
                        display: 'inline-block',
                        position: 'relative',
                        width: this.saturated.width(),
                        height: this.saturated.height()
                    });
                    
            if($.browser.msie && $.browser.version < 8) {
                this.wrapper.css({
                    display: 'inline',
                    zoom: 1
                });
            }
            
            this.saturated.wrap(this.wrapper);           
            this.desaturated.insertAfter(this.saturated);
            var positionalCss = {position: 'absolute', top: 0, left: 0}            
            this.saturated.css($.extend({zIndex: 2, opacity: 0}, positionalCss));
            this.desaturated.css($.extend({zIndex: 1}, positionalCss));
            
            this.bindEvents();
        };
        
        this.generateDesaturatedImage = function(saturated) {
            saturated = $(saturated);
            var desaturated = new Image();
            $(desaturated).load(function() {
                this.width = saturated.width();
                this.height = saturated.height();
                Pixastic.process(this, "desaturate", {average: false});
            });
            desaturated.src = saturated.attr('src');
            return $(desaturated);
        };
        
        this.bindEvents = function() {
            this.saturated.on("mouseenter", function(e) {
                self.saturated.stop().animate({opacity: 1}, self.settings.speed);
            }).on("mouseleave", function(e) {
                self.saturated.stop().animate({opacity: 0}, self.settings.speed);
            });
        };
        
        this.saturated = image;
        if(this.saturated.width() == 0) {
            this.saturated.bind("load", function(){
                self.init();
            });
        } else {
            this.init();
        }
    };
    
    
    $.fn.desaturate = function(options) {
        return this.each(function() {
            var image = $(this);
            if(!image.is('img') || image.data('Desaturate')) return;
            var d = new Desaturate(image, options);
            image.data('Desaturate', d);
        });
    };
})(jQuery, window);
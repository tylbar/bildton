<?php
/**
 * Template Name: Single Product Template
 *
 * Theme Name: BildTon mobile
 * Author: Tyler Barnes
 *
 */


get_header(); ?>

		<div id="content" class='single-product-page-mobilepress'>
			<div id="galery"><img id="galery-full" src="<?php the_field('image_1');?>" width="300" />
				<div id="galery-tumb">
					<?php if (get_field('image_1')):?><img class='img-selected img-selector' src="<?php the_field('image_1');?>" width="90" /><?php endif; ?>
					<?php if (get_field('image_2')):?><img class='img-selector' src="<?php the_field('image_2');?>" width="90" /><?php endif; ?>
					<?php if (get_field('image_3')):?><img class='img-selector' src="<?php the_field('image_3');?>" width="90" /><?php endif; ?>
					<?php if (get_field('image_4')):?><img class='img-selector' src="<?php the_field('image_4');?>" width="90" /><?php endif; ?>
					<?php if (get_field('image_5')):?><img class='img-selector' src="<?php the_field('image_5');?>" width="90" /><?php endif; ?>
					<?php if (get_field('image_6')):?><img class='img-selector' src="<?php the_field('image_6');?>" width="90" /><?php endif; ?>
				</div>
			</div>
			<div class="single-product-info-wrapper">
				<div id="single-product-header">
					<div class="single-product-price-wrapper clearfix">
						<div class="single-product-price-info-wrapper">
							<h1><?php the_title();?></h1>
							<p id='single-product-price'><span>Mietpreis:</span> <?php echo $price = get_field('product_price');?> EUR</p>
							<p id='single-product-price-sub'>(inkl. MwSt: <?php echo number_format ($price*1.19, 2);//the_field('inkl_mwst');?> EUR)<br/>
																(enthaltene MwSt: <?php echo number_format ($price*.19, 2);//the_field('enthaltene_mwst');?> EUR)</p>
						</div>
						<p id='single-product-price-pdf'><a href="<?php the_field('pdf');?>" target="_blank"><img src="<?php bloginfo('template_url');?>/images/pdf-logo.png" /></a></p>
					</div>

				</div>
				<div class="clear-fix"></div>

				<ul class="single-product-info-sections">
					<?php if(the_field('product_text')): ?>
					<li><hr></li>
					<li class='info-selector'>
						<p>Lieferumfang <img src="<?php echo get_template_directory_uri(); ?>/img/arrow.svg" alt="right arrow" class='right-arrow'></p>
						<div class="single-product-content">
							<p><?php the_field('product_text');?></p>
						</div>
					</li>
					<?php endif; ?>
					<li><hr></li>
					<li class='info-selector'>
						<p>Produktbeschreibung <img src="<?php echo get_template_directory_uri(); ?>/img/arrow.svg" alt="right arrow" class='right-arrow'></p>
						<div class='single-product-content'>
							<?php the_post(); ?>

							<?php the_content(); ?>
						</div>
					</li>
				</ul>

			</div>


		</div>
	</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>

<div id="sidebar-body">
			<?php if ( ! dynamic_sidebar( 'sidebar-1' ) ) : ?>
				<div id="sidebar-body-title">Service</div>
				<div id="sidebar-body-content">
					<div class="sidebar-body-content-box clearfix">
						<a class='sidebar-body-content-image-wrap' href="<?php bloginfo('url');?>/reparaturservice-muenchen/"><img src="<?php echo get_template_directory_uri(); ?>/images/reperaturserive.jpg" width="88" height="82" /></a>
            <div class="sidebar-body-content-text-wrap">
              <div class="sidebar-label-text"><a href="<?php bloginfo('url');?>/reparaturservice-muenchen/">Reparatur<br /><span class="">Equipment kaputt?<br />Wir reparieren es!</span></a></div>
  						<div class="clear-fix"></div>
            </div>
					</div>
					<div class="sidebar-body-content-box clearfix">
						<a class='sidebar-body-content-image-wrap' href="<?php bloginfo('url');?>/24htelefonsupport/"><img src="<?php echo get_template_directory_uri(); ?>/images/ruckgabe.png" width="88" height="88" /></a>
            <div class="sidebar-body-content-text-wrap">
              <div class="sidebar-label-text"><a href="<?php bloginfo('url');?>/24htelefonsupport/">24h-erreichbar<br /><span class="">Sie können uns 24h<br />erreichen!</span></a></div>
  						<div class="clear-fix"></div>
            </div>
					</div>
				</div>
			<?php endif; // end sidebar widget area ?>
			</div>

<?php get_header(); ?>


		<div id="content">
			<?php the_post(); ?>

			<?php get_template_part( 'template-parts/content', 'page' ); ?>
			<div class="seperator"></div>


		<?php if (is_front_page()):

			$pages = get_field('special_products');
			if($pages){ ?>
			<div class="products clearfix">
			<?php foreach($pages as $page){ ?>
			<div class="product-box">
				<div class="product-image-wrapper">
					<a class="ajax" href="<?php echo get_page_link($page->ID) ?>"><img src="<?php if  (get_field('product_image_tumb', $page->ID)) echo get_field('product_image_tumb', $page->ID); ?>" width="170" height="120" /></a>
				</div>
				<div class="product-title"><?php
				$string = $page->post_title;
				if (strlen($string) > 15) {
   $string = substr($string, 0, 15) . "..."; }
	 echo $string;
				 ?></div>
				<p class="product-button"><span class="price"><?php the_field('product_price', $page->ID);?> EUR / Tag</span> <a class="btn btn-primary ajax" href="<?php echo get_page_link($page->ID) ?>"><img class='mehr-button' src="<?php echo get_stylesheet_directory_uri(); ?>/img/mehr.png" alt="mehr button"></a></p>
			</div>
			<?php }	?>
			<div class="clearfix"></div>
			<?php }	?>
			</div>
		<?php endif; ?>
		</div>
	</div>

<?php get_footer(); ?>

		<?php if(is_page_template('page-single-product.php')): ?>
			<div class="seperator"></div>
			<div id="label">
				<div class="label-inner">
					<div class="label-box">
						<img src="<?php echo get_template_directory_uri(); ?>/images/phone.png" width="63" height="63" />
						<div class="label-text">BUCHEN UNTER<br /><span>+49 89 / 452 4499 0</span></div>
						<div class="clearfix"></div>
					</div>
					<div class="label-box">
						<a href="<?php if(get_field('pdf_file')) echo the_field('pdf_file');?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/pdf.png" width="63" height="63" /></a>
						<div class="label-text">AKTUELLE PREISE<br /><span class=""><a href="<?php if(get_field('pdf_file')) echo the_field('pdf_file');?>" target="_blank">PDF Download</a></span></div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="seperator"></div>
		<?php endif; ?>

		<?php get_template_part('template-parts/sidebar', 'body'); ?>
		<div class="seperator"></div>

		<div id="footerwrap">
			<div id="footer">
				<p>&copy; <?php echo date('Y'); ?> Copyright | All Rights Reserved.</p>
			</div>
		</div>

	</body>
</html>
